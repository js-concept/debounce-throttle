const input = document.querySelector("input");
const defaultText = document.getElementById("default");
const debounceText = document.getElementById("debounce");
const throttleText = document.getElementById("throttle");

const defaultCountText = document.getElementById("defaultCount");
const debounceCountText = document.getElementById("debounceCount");
const throttleCountText = document.getElementById("throttleCount");

const updateDebounceText = debounce((text) => {
  debounceText.textContent = text;
});

const updateThrottleText = throttle((text) => {
  throttleText.textContent = text;
});

input.addEventListener("input", (e) => {
  defaultText.textContent = e.target.value;
  updateDebounceText(e.target.value);
  updateThrottleText(e.target.value);
});

const updateDebounceCount = debounce(() => {
  incrementCount(debounceCountText);
});

const updateThrottleCount = throttle(() => {
  incrementCount(throttleCountText);
});

document.addEventListener("mousemove", (e) => {
  incrementCount(defaultCountText);
  updateDebounceCount();
  updateThrottleCount();
});

function incrementCount(element) {
  element.textContent = (parseInt(element.innerText) || 0) + 1;
}

function debounce(callBack, delay = 1000) {
  let timeOut;

  return (...args) => {
    clearTimeout(timeOut);
    timeOut = setTimeout(() => {
      callBack(...args);
    }, delay);
  };
}

function throttle(callBack, delay = 1000) {
  let shouldWait = false;
  let waitingArgs;

  const timeOutFunc = () => {
    if (waitingArgs == null) {
      shouldWait = false;
    } else {
      callBack(...waitingArgs);
      waitingArgs = null;
      setTimeout(timeOutFunc, delay);
    }
  };

  return (...args) => {
    if (shouldWait) {
      waitingArgs = args;
      return;
    }

    callBack(...args);
    shouldWait = true;

    setTimeout(timeOutFunc, delay);
  };
}
